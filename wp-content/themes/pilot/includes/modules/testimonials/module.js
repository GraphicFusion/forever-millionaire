jQuery(document).ready( function($) {
	$('.testimonials').slick({
		'autoplay': true,
		'arrows': false,
		'dots': true,
		'slidestoshow': 1,
		'fade': true,
		'easing': 'ease-in-out'
	});
});