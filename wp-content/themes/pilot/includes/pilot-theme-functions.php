<?php
	function pilot_get_title(){
		if (is_home()) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	function acf_load_cdn_field_choices( $field ) {
	    $field['choices'] = array();
		$type = get_field('theme_type', 'option');
		$gender = get_field('theme_gender', 'option');
		$url = 'http://fitmaster.wpengine.com/wp-json/wp/v2/media?filter[media_category]='.$type.'%2B'.$gender;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		$media = json_decode($output);
		foreach( $media as $media ){
			$url = $media->source_url;
			$field['choices'][$url] = "<img width='100px' src='".$url."'>";
		}
		curl_close($ch);    
		return $field;   
	}
	add_filter('acf/load_field/name=cdn_image', 'acf_load_cdn_field_choices');
	
	function asset_path($filename) {
		$dist_path = get_template_directory_uri() . DIST_DIR;
		$directory = dirname($filename) . '/';
		$file = basename($filename);
		static $manifest;
		
		if (empty($manifest)) {
			$manifest_path = get_template_directory() . DIST_DIR . 'assets.json';
			$manifest = new JsonManifest($manifest_path);
		}
		if (array_key_exists($file, $manifest->get())) {
			return $dist_path . $directory . $manifest->get().array($file);
		} else {
			return $dist_path . $directory . $file;
		}
	}

	// Custom styling for Login Page

	function login_styles() {
	?>
		<style type="text/css"> 
			body.login div#login h1 a {
				background-image: url( <?php echo site_url(); ?>/wp-content/themes/pilot/image/Logo.svg);
				background-size: 140px auto;
				width: 140px;
			}
		</style>
	<?php
	} 
	add_action( 'login_enqueue_scripts', 'login_styles' );

	if( array_key_exists('report_query', $_GET )){
		$start = 0;
  		if( array_key_exists('start', $_GET ) && $_GET['start']){
  			$start = $_GET['start'];
  		}
  		$end = date('m/d/Y');
  		if( array_key_exists('end', $_GET ) && $_GET['end']){
  			$end = $_GET['end'];
  		}
  		$args = [
    'date_query' => [
        [ 'before'  => date('Y-m-d',strtotime($end)), 'inclusive' => true ],
        [ 'after'  => date('Y-m-d',strtotime($start)), 'inclusive' => true ],
    ] 
];

$query = new WP_User_Query( $args );
$data_array = [];
$csv = "";
foreach( $query->results as $user ){
//	print_r($user);
	$quiz = get_user_meta($user->ID, 'textarea');
	//print_r($quiz);
	$data_array[] = [$user->ID,$user->data->user_registered, $quiz[0]]; 
    $csv.= "------------------------------------------------------------------------------------------ \n USER ID: ".$user->ID."\n USERNAME: ".$user->data->user_nicename."\n USER EMAIL: ".$user->data->user_email."\n DATE REGISTERED:".$user->data->user_registered."\n QUIZ RESULTS: \n". $quiz[0] ."\n\n\n\n"; //Append data to csv

}


//$csv = "id,registeredDate,quiz \n";//Column headers
foreach ($data_array as $record){
//    $csv.= "-------------------------------------------------------------------- \n USER ID: ".$record[0].'\n USERNAME: '.$record[1]."\n".$record[2]."\n"; //Append data to csv
}

$csv_handler = fopen (__DIR__.'\report.txt','w');
fwrite ($csv_handler,$csv);
fclose ($csv_handler);

//echo __DIR__.'report.txt';
//exit(); 
 wp_redirect( "/assessment-reports/?start=".$_GET['start']."&end=".$_GET['end'] );
	}

add_action( 'wp_ajax_nopriv_ajax_hub', 'ajax_hub' );
add_action( 'wp_ajax_ajax_hub', 'ajax_hub' );
function ajax_hub(){
	$params = $_REQUEST['params'];

	echo json_encode( $params );
	exit;
}

?>