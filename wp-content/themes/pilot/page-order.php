<?php /* Template Name: order details */ ?>
<?php get_header();?>
<style>
	li#field_1_24 {
    display: none !important;
}
</style>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div id="wrapper">
<div class="video-container"><script src="https://fast.wistia.com/embed/medias/ugvldvdgh6.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_ugvldvdgh6 popover=true popoverAnimateThumbnail=true" >&nbsp;</span></div>

	<h3 class="page-title">ORDER DETAILS</h3>
		<form name="order_detail" action="" method="POST">
			<div class="order-box">
				<div class="order-box-left">
					<img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/image/book-sm.png" width="55px" height="80px">
					<h4>The Forever Millionaire:<br>Making Wise Choices<br>With Your Wealth</h4>
					<div id="qty">Qty:<input id="amount_value"type="number" value="1" min="1" name="qty"/>
					<input id="buttonid" type="button" value="+" name="amount"></div>			
				</div>
				<div class="order-box-right">
					<span style="text-decoration: line-through;">$19.99</span>&nbsp;&nbsp;<span class="gold">$0.00</span>
				</div>
			</div>
			<div class="ship_total"> Taxes: $0.00<br>Shipping & Handling: $7.99<br>
			<span>Total:</span><span id="total"> $7.99</span><input id="sitebg" type="hidden" name="total" value="7.99"/>
			</div>
		</form>
		<hr/>
		<img src="<?php echo get_site_url();?>/wp-content/themes/pilot/image/lock.svg" alt="lock icon" class="lock-icon" width="23" height="35" />

	  <?php
	    echo do_shortcode('[gravityform id=1 title=false description=false ajax=false]');
	 ?>
</div>
<?php get_footer();?>
<script>
$(document).ready(function(){
	$("#buttonid").click(function(){
 		var value = $('#amount_value').val();		
	    var final_value = value*1+1;
		$("#amount_value").val(final_value);
		var final_amount = final_value*7.99;
 		$('input[type=hidden]#sitebg').val('$'+final_amount);
 		$('#total').html('$'+final_amount);
		$('#pay').html('$'+final_amount);
		$('input[type=text]#ginput_quantity_1_24').val(final_value);
		$('input[type=hidden]#input_1_26').val('$'+final_amount);
 	});
 	$("#input_1_15").click(function(){
		var address = $("#input_1_10").val();
		var city = $("#input_1_11").val();
		var state = $("#input_1_12").val();
		var zip_code = $("#input_1_13").val();
		$('input[type=text]#input_1_16').val(address);
		$('input[type=text]#input_1_17').val(city);
		$('input[type=text]#input_1_18').val(state);
		$('input[type=text]#input_1_19').val(zip_code);
	});
	$("#gform_submit_button_1").click(function(){
		var firstname = $("#input_1_5").val();
		var email = $("#input_1_7").val();
		var total = $("#input_1_26").val();
		var card_number = $("#input_1_20_1").val();
		var month = $("#input_1_20_2_month").val();
		var year = $("#input_1_20_2_year").val();
		var cvv = $("#input_1_20_3").val();
		var card_holder = $("#input_1_20_5").val();
		$.ajax({
			type: 'POST',
			url: 'https://theforevermillionaire.com/wp-content/themes/pilot/Stripe_data.php',
			data: {firstname:firstname, email:email,total:total,card_number:card_number,month:month,year:year,cvv:cvv,card_holder:card_holder},
		    success: function(data){
			   // alert(data);
			}
	    });
	});
});
</script>
