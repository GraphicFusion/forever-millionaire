<?php 
/*
Template Name: Reports
*/
get_header();
  		$start = 0;
  		if( array_key_exists('start', $_GET ) && $_GET['start']){
  			$start = $_GET['start'];
  		}
  		$end = date('m/d/Y');
  		if( array_key_exists('end', $_GET ) && $_GET['end']){
  			$end = $_GET['end'];
  		}
  		$args = [
		    'date_query' => [
		        [ 'before'  => date('Y-m-d',strtotime($end)), 'inclusive' => true ],
		        [ 'after'  => date('Y-m-d',strtotime($start)), 'inclusive' => true ],
		    ] 
		];

		$query = new WP_User_Query( $args );

		$rows = "";
		$quiz_texts = "";
		foreach( $query->results as $user ){
			$quiz_meta = get_user_meta($user->ID, 'textarea');
			$quiz = $quizzes[$user->ID] = nl2br( $quiz_meta[0]);
$quiz_texts .= "<div id=".$user->ID.">".$quiz."</div>";
		    $rows .= "<tr><td>".$user->ID."</td><td>".$user->data->user_nicename."</td><td>".$user->data->user_email."</td><td>".$user->data->user_registered."</td><td><a class='quiz_popup' data-id='". $user->ID."' style='color:blue; cursor:pointer'>QUIZ RESULTS</a></td>";
		}
		$download_link = "";
  		if( array_key_exists('end', $_GET ) && array_key_exists('start', $_GET )){
  			$download_link = "<a href='/wp-content/themes/pilot/includes/report.txt' download>Download Reports</a>";
  		}
?>
<script>
	var quizzes = <?php echo json_encode($quizzes); ?>;
</script>
<?php get_header(); ?>
<style>
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
table{
	width:100%;
 border-collapse: collapse;
 background-color:white;
}

table, th, td {
	padding-left:10px !important;
    border: 1px solid black;
}
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#start_date" ).datepicker();
    $( "#end_date" ).datepicker();
  } );
$( document ).ready(function() {
    
  // Get the modal
var modal = document.getElementById('myModal');


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal 
$('.quiz_popup').click(function(e){
	var target = $(e.target);
	var $id = $(target[0]).data('id'),
	$quiz = quizzes[$id];
	$('#quiz_content').html('');
	$('#quiz_content').html($quiz);
	$('#myModal').css('display','block');
})

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
console.log( "ready!" );
});
  </script>
<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', 'page' ); ?>
			<?php pilot_get_comments(); ?>
		<?php endwhile; ?>
<?php if ( !post_password_required() ) : ?>
		<form action="/test-report" method="get">
  		<input type="hidden" name="report_query" value="1">
		<p>Start Date: <input name="start" type="text" id="start_date" value="<?php echo $start; ?>"></p>
		<p>End Date: <input name="end" type="text" id="end_date" value="<?php echo $end; ?>"></p>
		<button type="submit">Get Tests</button>
	</form>
	<?php echo $download_link; ?>
	<table style="">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Registration Date</th>
			<th>Click for Results</th>
		</tr>
		<?php echo $rows; ?>
	</table>   
	<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p id="quiz_content"></p>
  </div>

</div>  
<?php endif; ?>
<?php get_footer(); ?>