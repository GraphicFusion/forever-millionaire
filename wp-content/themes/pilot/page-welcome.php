<?php 
/* Template Name: Welcome*/ 
?>
<style>
.page-template-page-welcome .site-content {
    background-color: white;
    margin-top: 68px;
    padding: 50px;
    margin-bottom: 50px;
}	
</style>
<?php get_header();?>
<div class="thanks">
<div class="thank-you-box" style="max-width:1000px !important; margin-left:-50px">
<div><img src="https://shepherd.sonderdev.com/wp-content/uploads/2017/10/success-checkmark.svg" alt="" width="60" height="60" /><h3 style="display:inline-block; font-size:12px;">Thank You For Your Order. You will receive a receipt in your email.</h3></div>
<div class="close">X</div>
</div>
</div>


<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', 'page' ); ?>
			<?php pilot_get_comments(); ?>
		<?php endwhile; ?>
<?php get_footer();?>
