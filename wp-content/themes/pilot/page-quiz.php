<?php
/*
Template Name: Quiz
*/
get_header(); 
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

$(document).ready(function (){


	$("#investment").hide();
	$("#retirement").hide();
	$("#taxes").hide();
	$("#assistance").hide();
	$("#estate_plan").hide();
	$("#insurance").hide();
	$("#case_flow").hide();
	
	var val = 14+'%';  //pregress bar
	$('.progress-bar').width(val);
	$('.progress-bar span').text(val);
	$("#start_quiz").click(function (){			
	    gtag('event', 'start-the-assessment', { 'send_to': 'UA-110503781-1' });
	    var first_name = $("#first_name").val();
	    var last_name = $("#last_name").val();
	    var email = $("#email").val();
		if(first_name == ""){
		  $("#first_label").text("Please enter the first name");	
		}
		if(last_name == ""){
			$("#last_label").text("Please enter the last name");
		}
		if(email == ""){
			$("#email_label").text("Please enter the email");
		}
		if(first_name !== "" && last_name !== "" && email !== ""){
			var val = 19+'%';  //pregress bar
			$('.progress-bar').width(val);
			$('.progress-bar span').text(val);
			$("#investment").show("slide");
		    gtag('event', 'investments', { 'send_to': 'UA-110503781-1' });  
			$("#quiz").hide();
		}
		else{
		  return false;
		}
	  });
	$("#button-gold_investment").click(function () {
		var val = 32+'%';  //pregress bar
		$('.progress-bar').width(val);
		$('.progress-bar span').text(val);
		$("#retirement").show("slide");
	    gtag('event', 'retirement', { 'send_to': 'UA-110503781-1' });
	$("#investment").hide();
	});
	$("#button-gold_retirement").click(function () {
		var val = 48+'%';  //pregress bar
		$('.progress-bar').width(val);
		$('.progress-bar span').text(val);
		$("#taxes").show("slide");
	    gtag('event', 'taxes', { 'send_to': 'UA-110503781-1' });
		$("#retirement").hide();
	
	});
	$("#button-gold_taxes").click(function () {
		var val = 62+'%';  //pregress bar
		$('.progress-bar').width(val);
		$('.progress-bar span').text(val);
		$("#assistance").show("slide");
	    gtag('event', 'assistance-to-loved-ones', { 'send_to': 'UA-110503781-1' });     
		$("#taxes").hide();
	});
	$("#button-gold_assistance").click(function () {
		var val = 75+'%';  //pregress bar
		$('.progress-bar').width(val);
		$('.progress-bar span').text(val);
		$("#estate_plan").show("slide");
	    gtag('event', 'estate-planning', { 'send_to': 'UA-110503781-1' });     
		$("#assistance").hide();
	});
	$("#button-gold_estate_planinng").click(function () {
		var val = 90+'%';  //pregress bar
		$('.progress-bar').width(val);
		$('.progress-bar span').text(val);
		$("#insurance").show("slide");
	    gtag('event', 'insurance-and-protection', { 'send_to': 'UA-110503781-1' });     
		$("#estate_plan").hide();
	});
	$("#button-gold_insurance").click(function () {
		var val = 97+'%';  //pregress bar
		$('.progress-bar').width(val);
		$('.progress-bar span').text(val);
		$("#case_flow").show("slide");
	    gtag('event', 'cash-flow-and-budgeting', { 'send_to': 'UA-110503781-1' });     
		$("#insurance").hide();
	});
	
	//For radio button checked condition
	
	//YOUR INVESTMENTS SECTION
	$("#button-gold_investment").addClass("disabled");
	$(".rating input:radio").each(function(){
	    var final_length = $('.rating_radio:checked').length;
	    if(final_length == 3){			
			$("#button-gold_investment").removeClass("disabled");
		}
	}); 

    $(".rating input:radio").change(function(){
		var final_length = $('.rating_radio:checked').length;
		if(final_length == 3){			
			$("#button-gold_investment").removeClass("disabled");
		}
    });
	
	
	//RETIREMENT SECTION
	$("#button-gold_retirement").addClass("disabled");
	$(".rating1 input:radio").each(function(){
	    var final_length = $('.rating_radio1:checked').length;
	    if(final_length == 5){
			$("#button-gold_retirement").removeClass("disabled");
        }		
	}); 
    $(".rating1 input:radio").change(function(){
		var final_length = $('.rating_radio1:checked').length;
		if(final_length == 5){
			$("#button-gold_retirement").removeClass("disabled");
		}
    });
	
	//TAXES SECTION	
	$("#button-gold_taxes").addClass("disabled");
	$(".rating2 input:radio").each(function(){
	    var final_length = $('.rating_radio2:checked').length;
	    if(final_length == 5){
			$("#button-gold_taxes").removeClass("disabled");
        }		
	}); 
    $(".rating2 input:radio").change(function(){
		var final_length = $('.rating_radio2:checked').length;
		if(final_length == 5){
			$("#button-gold_taxes").removeClass("disabled");
		}
    });
	
	//ASSISTANCE TO LOVES ONES
	$("#button-gold_assistance").addClass("disabled");
	$(".rating3 input:radio").each(function(){
	    var final_length = $('.rating_radio3:checked').length;
	    if(final_length == 3){			
			$("#button-gold_assistance").removeClass("disabled");
        }		
	}); 
    $(".rating3 input:radio").change(function(){
		var final_length = $('.rating_radio3:checked').length;
		if(final_length == 3){
			$("#button-gold_assistance").removeClass("disabled");
		}
    });
	
	//ESTATE PLANNING
	$("#button-gold_estate_planinng").addClass("disabled");
	$(".rating4 input:radio").each(function(){
	    var final_length = $('.rating_radio4:checked').length;
	    if(final_length == 3){			
			$("#button-gold_estate_planinng").removeClass("disabled");
        }		
	}); 
    $(".rating4 input:radio").change(function(){
		var final_length = $('.rating_radio4:checked').length;
		if(final_length == 3){
			$("#button-gold_estate_planinng").removeClass("disabled");
		}
    });
	
	//INSURANCE & PROTECTION
	$("#button-gold_insurance").addClass("disabled");
	$(".rating5 input:radio").each(function(){
	    var final_length = $('.rating_radio5:checked').length;
	    if(final_length == 5){
			$("#button-gold_insurance").removeClass("disabled");
        }		
	}); 
    $(".rating5 input:radio").change(function(){
		var final_length = $('.rating_radio5:checked').length;
		if(final_length == 5){
			$("#button-gold_insurance").removeClass("disabled");
		}
    });
	
	//CASH FLOW AND BUDGETING
	$("#case_flow_button-gold").addClass("disabled");
	$(".rating6 input:radio").each(function(){
	    var final_length = $('.rating_radio6:checked').length;
	    if(final_length == 4){
			$("#case_flow_button-gold").removeClass("disabled");
        }		
	}); 
    $(".rating6 input:radio").change(function(){
		var final_length = $('.rating_radio6:checked').length;
		if(final_length == 4){
			$("#case_flow_button-gold").removeClass("disabled");
		}
    });
});
</script>

<div class="progress progress-striped active">
  <div class="progress-bar"><span></span></div>
</div>

<div class="thin-content">
	<form action="<?php echo get_site_url(); ?>/thank-you/" method="post">
		<div id="quiz">
			<h3 class="no-line">START THE ASSESSMENT</h3>
			<div class="rating0">
				<div class="quiz-item">
					<label for="first_name">First Name:</label><input type="text" name="first_name" id="first_name" value="" required/><span id="first_label" class="warning"></span>
				</div>
				<div class="quiz-item">
					<label for="last_name">Last Name:</label><input type="text" name="last_name" id="last_name" value="" required/><span id="last_label" class="warning"></span>
				</div>
				<div class="quiz-item">
					<label for="email">Email:</label><input type="email" name="email" value="" id="email" required/><span id="email_label" class="warning"></span>
				</div>
				<p style="margin-top: 30px;">Instructions: for the questions that follow, rate yourself on a scale from 1-5: 1
				being No, or that you are unhappy or you lack confidence in that area 
				and 5 being an absolute Yes or that you are happy with where you are or 
				confident you are on the right path.</p>
		  </div>		    
			<input type="button" name="start_quiz" class="button-gold" id="start_quiz" value="Next" />
		</div>
	</div> 
    <div id="investment" class="quiz-container">
		<h3>INVESTMENTS</h3>
		   <center>When it comes to your investments in general, on a scale of 1-5...<hr></center>
	       <center>How happy are you with how much money your investments are making?</center>
		   <input type="hidden" name="hide_investment" value="How happy are you with how much money your investments are making?" />
		<span class="rating">
			<div class="rating-group">
				<span class="rating-low">Unhappy</span>
				<div class="rating-input">
	      	<input id="rating1" type="radio" name="rating" value="1" class="rating_radio">
          <label for="rating1">1</label>
	      </div>
				<div class="rating-input">
					<input id="rating2" type="radio" name="rating" value="2" class="rating_radio">
          <label for="rating2">2</label>
				</div>
				<div class="rating-input">
      	 	<input id="rating3" type="radio" name="rating" value="3" class="rating_radio">
          <label for="rating3">3</label>
					      	 </div>
				<div class="rating-input">
					<input id="rating4" type="radio" name="rating" value="4" class="rating_radio">
          <label for="rating4">4</label>
				</div>
				<div class="rating-input">
					<input id="rating5" type="radio" name="rating" value="5" class="rating_radio">
					<label for="rating5">5</label>
				</div>
				<span class="rating-high">Happy</span>
      </div>
        <hr>         
		 <center>How confident are you that you will not outlive your money?</center>
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
				<input type="hidden" name="hide_investment1" value="How confident are you that you will not outlive your money?" />
				<div class="rating-input">
					<input id="rating_1" type="radio" name="rating1" value="1" class="rating_radio">
					          <label for="rating_1">1</label>
				</div>
				<div class="rating-input">
					<input id="rating_2" type="radio" name="rating1" value="2" class="rating_radio">
					          <label for="rating_2">2</label>
				</div>
				<div class="rating-input">
					<input id="rating_3" type="radio" name="rating1" value="3" class="rating_radio">
					          <label for="rating_3">3</label>
				</div>
				<div class="rating-input">
					<input id="rating_4" type="radio" name="rating1" value="4" class="rating_radio">
					          <label for="rating_4">4</label>
				</div>
				<div class="rating-input">
					<input id="rating_5" type="radio" name="rating1" value="5" class="rating_radio">
					<label for="rating_5">5</label>
				</div>
        <span class="rating-high">Confident</span>
      </div>

      <hr>		  
		  <center>How confident are you that you will have the money you will need to live on even in a bad market?</center>
		  <input type="hidden" name="hide_investment2" value="How confident are you that you will have the money you will need to live on even in a bad market?" />
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_10" type="radio" name="rating2" value="1" class="rating_radio">
							        <label for="rating_10">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_20" type="radio" name="rating2" value="2" class="rating_radio">
							        <label for="rating_20">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_30" type="radio" name="rating2" value="3" class="rating_radio">
							        <label for="rating_30">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_40" type="radio" name="rating2" value="4" class="rating_radio">
							        <label for="rating_40">4</label>
					</div>
					<div class="rating-input"><input id="rating_50" type="radio" name="rating2" value="5" class="rating_radio">
						<label for="rating_50">5</label>
					</div>
	      <span class="rating-high">Confident</span>
	    </div>     
	    </span><br><br>
		 <input type="button" id="button-gold_investment"  name="investment" class="button-gold" value="Next"/ >
	</div> 
    <div id="retirement" class="quiz-container">
		  <h3>RETIREMENT</h3>
		  <center>When it comes to your retirement, on a scale of 1-5...<hr></center>
	      <center>How confident are you that you’re on the right track to reach your retirement goals?</center>
		  <input type="hidden" name="hide_retirement" value="How confident are you that you’re on the right track to reach your retirement goals?" />
	    <span class="rating1">
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
						<div class="rating-input">
							<input id="rating_11" type="radio" name="rating3" value="1" class="rating_radio1">
							                <label for="rating_11">1</label>
						</div>
						<div class="rating-input">
							<input id="rating_21" type="radio" name="rating3" value="2" class="rating_radio1">
							                <label for="rating_21">2</label>
						</div>
						<div class="rating-input">
							<input id="rating_31" type="radio" name="rating3" value="3" class="rating_radio1">
							                <label for="rating_31">3</label>
						</div>
						<div class="rating-input">
							<input id="rating_41" type="radio" name="rating3" value="4" class="rating_radio1">
							                <label for="rating_41">4</label>
						</div>
						<div class="rating-input">
							<input id="rating_51" type="radio" name="rating3" value="5" class="rating_radio1">
							<label for="rating_51">5</label>
						</div>
					<span class="rating-high">Confident</span>
				</div>

        <hr>
		  
		    <center>How confident are you that you’re on the right track to maintain your lifestyle during retirement?</center>
			 <input type="hidden" name="hide_retirement1" value="How confident are you that you’re on the right track to maintain your lifestyle during retirement?" />
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_12" type="radio" name="rating4" value="1" class="rating_radio1">
								                <label for="rating_12">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_22" type="radio" name="rating4" value="2" class="rating_radio1">
								                <label for="rating_22">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_32" type="radio" name="rating4" value="3" class="rating_radio1">
								                <label for="rating_32">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_42" type="radio" name="rating4" value="4" class="rating_radio1">
						                <label for="rating_42">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_52" type="radio" name="rating4" value="5" class="rating_radio1">
						<label for="rating_52">5</label>
					</div>
					<span class="rating-high">Confident</span>
			</div>

      <hr>
		  
		   <center>How confident are you that after taxes, you’re putting the most money in your pocket you can from your current (or future) portfolio   withdrawals?</center>
		   <input type="hidden" name="hide_retirement2" value="How confident are you that after taxes, you’re putting the most money in your pocket you can from your current (or future) portfolio   withdrawals?" />
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_13" type="radio" name="rating5" value="1" class="rating_radio1">
							                <label for="rating_13">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_23" type="radio" name="rating5" value="2" class="rating_radio1">
							                <label for="rating_23">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_33" type="radio" name="rating5" value="3" class="rating_radio1">
							                <label for="rating_33">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_43" type="radio" name="rating5" value="4" class="rating_radio1">
						          <label for="rating_43">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_53" type="radio" name="rating5" value="5" class="rating_radio1">
						<label for="rating_53">5</label>
					</div>
					<span class="rating-high">Confident</span>
			</div>

			<hr>
		  
		   <center>How confident are you that such withdrawals are appropriate and don’t impair the future growth of your portfolio?</center>
		   <input type="hidden" name="hide_retirement3" value="How confident are you that such withdrawals are appropriate and don’t impair the future growth of your portfolio?" />
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_14" type="radio" name="rating6" value="1" class="rating_radio1">
									    <label for="rating_14">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_24" type="radio" name="rating6" value="2" class="rating_radio1">
						                <label for="rating_24">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_34" type="radio" name="rating6" value="3" class="rating_radio1">
						                <label for="rating_34">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_44" type="radio" name="rating6" value="4" class="rating_radio1">
						                <label for="rating_44">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_54" type="radio" name="rating6" value="5" class="rating_radio1">
						<label for="rating_54">5</label>
					</div>
					<span class="rating-high">Confident</span>
			</div>
<hr>	 	  
		  <center>Do you have an inspiring bucket list for what you want to do in retirement?</center>
		  <input type="hidden" name="hide_retirement4" value="Do you have an inspiring bucket list for what you want to do in retirement?" />
			<div class="rating-group">
				<span class="rating-low">No</span>
					<div class="rating-input">
						<input id="rating_14" type="radio" name="rating_list_retirement" value="1" class="rating_radio1">
									    <label for="rating_14">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_24" type="radio" name="rating_list_retirement" value="2" class="rating_radio1">
						                <label for="rating_24">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_34" type="radio" name="rating_list_retirement" value="3" class="rating_radio1">
						                <label for="rating_34">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_44" type="radio" name="rating_list_retirement" value="4" class="rating_radio1">
						                <label for="rating_44">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_54" type="radio" name="rating_list_retirement" value="5" class="rating_radio1">
						<label for="rating_54">5</label>
					</div>
					<span class="rating-high">Yes</span>
			</div>

			<br><br>
		
		  <input type="button" id="button-gold_retirement" name="retirement" class="button-gold" value="Next"/ >
		</span>
	</div>
	<div id="taxes" class="quiz-container">
		   <h3>TAXES</h3>
		   <center>When it comes to your taxes, on a scale of 1-5<hr></center>
		   <center>How confident are you that you are paying the lowest amount of taxes that is legally possible?</center>
		    <input type="hidden" name="hide_taxes" value="How confident are you that you are paying the lowest amount of taxes that is legally possible?" />
		   <span class="rating2">
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
						<div class="rating-input">
							<input id="rating_15" type="radio" name="rating7" value="1" class="rating_radio2">
							                <label for="rating_15">1</label>
						</div>
						<div class="rating-input">
							<input id="rating_25" type="radio" name="rating7" value="2" class="rating_radio2">
							                <label for="rating_25">2</label>
						</div>
						<div class="rating-input">
							<input id="rating_35" type="radio" name="rating7" value="3" class="rating_radio2">
							                <label for="rating_35">3</label>
						</div>
						<div class="rating-input">
							<input id="rating_45" type="radio" name="rating7" value="4" class="rating_radio2">
              <label for="rating_45">4</label>
            </div>
            <div class="rating-input">
              <input id="rating_55" type="radio" name="rating7" value="5" class="rating_radio2">
              <label for="rating_55">5</label>
						</div>
					<span class="rating-high">Confident</span>
				</div>

				<hr>
		  
		        <center>How confident are you that you and your tax professional, in coordination with your financial advisor, have evaluated and implemented all tax-savings strategies that could put more money back in your pocket?</center>
				<input type="hidden" name="hide_taxes1" value="How confident are you that you and your tax professional, in coordination with your financial advisor,have evaluated and implemented all tax-savings strategies that could put more money back in your pocket?" />
		   <span class="rating2">
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
				<div class="rating-input">
					<input id="rating_16" type="radio" name="rating8" value="1" class="rating_radio2">
					          <label for="rating_16">1</label>
				</div>
				<div class="rating-input">
					<input id="rating_26" type="radio" name="rating8" value="2" class="rating_radio2">
					                <label for="rating_26">2</label>
				</div>
				<div class="rating-input">
					<input id="rating_36" type="radio" name="rating8" value="3" class="rating_radio2">
					                <label for="rating_36">3</label>
				</div>
				<div class="rating-input">
					<input id="rating_46" type="radio" name="rating8" value="4" class="rating_radio2">
					                <label for="rating_46">4</label>
				</div>
				<div class="rating-input">
					<input id="rating_56" type="radio" name="rating8" value="5" class="rating_radio2">
					<label for="rating_56">5</label>
				</div>
				<span class="rating-high">Confident</span>
			</div>

			<hr>
		  
		        <center>How confident are you that your tax-savings strategies are being addressed for you on an ongoing basis?</center>
				<input type="hidden" name="hide_taxes2" value="How confident are you that your tax-savings strategies are being addressed for you on an ongoing basis?(If you don’t have tax-savings strategies, mark 0)" />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
						<div class="rating-input">
							<input id="rating_17" type="radio" name="rating9" value="1" class="rating_radio2">
							                <label for="rating_17">1</label>
						</div>
						<div class="rating-input">
							<input id="rating_27" type="radio" name="rating9" value="2" class="rating_radio2">
							                <label for="rating_27">2</label>
						</div>
						<div class="rating-input">
							<input id="rating_37" type="radio" name="rating9" value="3" class="rating_radio2">
							                <label for="rating_37">3</label>
						</div>
						<div class="rating-input">
							<input id="rating_47" type="radio" name="rating9" value="4" class="rating_radio2">
							                <label for="rating_47">4</label>
						</div>
						<div class="rating-input">
							<input id="rating_57" type="radio" name="rating9" value="5" class="rating_radio2">
							<label for="rating_57">5</label>
						</div>
					<span class="rating-high">Confident</span>
				</div>

                <hr>
		  
			    <center>Do you have an overall plan that minimizes your taxes over the next 5-10 years?</center>
				<input type="hidden" name="hide_taxes3" value="Do you have an overall plan that minimizes your taxes over the next 5-10 years?" />			
				<div class="rating-group">
					<span class="rating-low">No</span>
						<div class="rating-input">
							<input id="rating_17" type="radio" name="rating_list_plan" value="1" class="rating_radio2">
							                <label for="rating_17">1</label>
						</div>
						<div class="rating-input">
							<input id="rating_27" type="radio" name="rating_list_plan" value="2" class="rating_radio2">
							                <label for="rating_27">2</label>
						</div>
						<div class="rating-input">
							<input id="rating_37" type="radio" name="rating_list_plan" value="3" class="rating_radio2">
							                <label for="rating_37">3</label>
						</div>
						<div class="rating-input">
							<input id="rating_47" type="radio" name="rating_list_plan" value="4" class="rating_radio2">
							                <label for="rating_47">4</label>
						</div>
						<div class="rating-input">
							<input id="rating_57" type="radio" name="rating_list_plan" value="5" class="rating_radio2">
							<label for="rating_57">5</label>
						</div>
					<span class="rating-high">Yes</span>
				</div>

			   <!--textarea id="plan_list"></textarea----><hr>
		  
		         <center>To the extent that you may have investment losses in any given year, how confident are you that those losses are being strategically used to your benefit for tax purposes?</center>
				 <input type="hidden" name="hide_taxes4" value="To the extent that you may have investment losses in any given year, how confident are you that those losses are being strategically used to your benefit for tax purposes?" />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_18" type="radio" name="rating10" value="1" class="rating_radio2">
						                <label for="rating_18">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_28" type="radio" name="rating10" value="2" class="rating_radio2">
						                <label for="rating_28">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_38" type="radio" name="rating10" value="3" class="rating_radio2">
						                <label for="rating_38">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_48" type="radio" name="rating10" value="4" class="rating_radio2">
						                <label for="rating_48">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_58" type="radio" name="rating10" value="5" class="rating_radio2">
						<label for="rating_58">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

                <br><br> 
			
			    <input type="button" id="button-gold_taxes" name="taxes" class="button-gold" value="Next"/ >
		    </span>
	</div> 
		<div id="assistance" class="quiz-container">
		  <h3>ASSISTANCE TO LOVED ONES</h3>
		   <center>How confident are you that you have everything you need to handle any future financial or health care needs of your parents?
		   </center>
		   <input type="hidden" name="hide_assistance" value="How confident are you that you have everything you need to handle any future financial or health care needs of your parents?" />
		   <span class="rating3">
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_19" type="radio" name="rating11" value="1" class="rating_radio3">
						          <label for="rating_19">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_29" type="radio" name="rating11" value="2" class="rating_radio3">
						                <label for="rating_29">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_39" type="radio" name="rating11" value="3" class="rating_radio3">
						                <label for="rating_39">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_49" type="radio" name="rating11" value="4" class="rating_radio3">
						                <label for="rating_49">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_59" type="radio" name="rating11" value="5" class="rating_radio3">
						<label for="rating_59">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

        <hr>
		  
				<center>How confident are you that you’re maximizing any tax benefits for yourself or your beneficiaries when you are helping your children or grandchildren pay for education, physical needs or other areas where you want to help them? 
				</center>
				 <input type="hidden" name="hide_assistance1" value="How confident are you that you’re maximizing any tax benefits for yourself or your beneficiaries when you are helping your children or grandchildren pay for education, physical needs or other areas where you want to help them? " />
			<div class="rating-group">
				<span class="rating-low">Unconfident</span>
				<div class="rating-input">
					<input id="rating_120" type="radio" name="rating12" value="1" class="rating_radio3">
					                <label for="rating_120">1</label>
				</div>
				<div class="rating-input">
					<input id="rating_220" type="radio" name="rating12" value="2" class="rating_radio3">
					                <label for="rating_220">2</label>
				</div>
				<div class="rating-input">
					<input id="rating_320" type="radio" name="rating12" value="3" class="rating_radio3">
					                <label for="rating_320">3</label>
				</div>
				<div class="rating-input">
					<input id="rating_420" type="radio" name="rating12" value="4" class="rating_radio3">
					                <label for="rating_420">4</label>
				</div>
				<div class="rating-input">
					<input id="rating_520" type="radio" name="rating12" value="5" class="rating_radio3">
					<label for="rating_520">5</label>
				</div>
				<span class="rating-high">Confident</span>
			</div>

        <hr>
		  
				<center>How confident are you that you’ve fully understand how any monetary gifts or ongoing help to your children or grandchildren affects their dependency on you instead of maximizing their self-reliance?
				</center>		  
				 <input type="hidden" name="hide_assistance2" value="How confident are you that you’ve fully understand how any monetary gifts or ongoing help to your children or grandchildren affects their dependency on you instead of maximizing their self-reliance? " />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating_121" type="radio" name="rating13" value="1" class="rating_radio3">
						                <label for="rating_121">1</label>
					</div>
					<div class="rating-input">
						<input id="rating_221" type="radio" name="rating13" value="2" class="rating_radio3">
						                <label for="rating_221">2</label>
					</div>
					<div class="rating-input">
						<input id="rating_321" type="radio" name="rating13" value="3" class="rating_radio3">
						                <label for="rating_321">3</label>
					</div>
					<div class="rating-input">
						<input id="rating_421" type="radio" name="rating13" value="4" class="rating_radio3">
						                <label for="rating_421">4</label>
					</div>
					<div class="rating-input">
						<input id="rating_521" type="radio" name="rating13" value="5" class="rating_radio3">
						<label for="rating_521">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

                <br><br>
	
				<input type="button" id="button-gold_assistance"  class="button-gold" name="assistance" value="Next"/ >
		    </span>
		</div> 
		<div  id="estate_plan" class="quiz-container">
			<h3>ESTATE PLANNING</h3>
			<span class="rating4">
			   <center>Is all your important financial information (trusts, policies, statements, passwords, etc.) organized and accessible in a way that makes it easy for your family or friends to know and execute your exact wishes? </center>
			   <input type="hidden" name="hide_estate_planning" value="Is all your important financial information (trusts, policies, statements, passwords, etc.) organized and accessible in a way that makes it easy for my family or friends to know and execute your exact wishes? " />				
			    <div class="rating-group">
					<span class="rating-low">No</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating_wishes" value="1" class="rating_radio4">
						<label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating_wishes" value="2" class="rating_radio4">
						<label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating_wishes" value="3" class="rating_radio4">
						<label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating_wishes" value="4" class="rating_radio4">
						<label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating_wishes" value="5" class="rating_radio4">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Yes</span>
				</div>
			  <hr>
			   <center>Are the people currently listed in your will or trust still the people you want to actually receive your money and property when you die? </center>
			   <input type="hidden" name="hide_estate_planning1" value="Are the people currently listed in your will or trust still the people you want to actually receive your money and property when you die? " />				
			    <div class="rating-group">
					<span class="rating-low">No</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating_die" value="1" class="rating_radio4">
						<label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating_die" value="2" class="rating_radio4">
						<label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating_die" value="3" class="rating_radio4">
						<label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating_die" value="4" class="rating_radio4">
						<label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating_die" value="5" class="rating_radio4">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Yes</span>
				</div>
			  <hr>
			   <center>How confident are you that your estate planning documents are up to date with tax and estate laws and do not need to be reviewed by an estate planning attorney?
			   </center>
			   <input type="hidden" name="hide_estate_planning2" value="How confident are you that your estate planning documents are up to date with tax and estate laws and do not need to be reviewed by an estate planning attorney? " />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating14" value="1" class="rating_radio4">
						<label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating14" value="2" class="rating_radio4">
						<label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating14" value="3" class="rating_radio4">
						<label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating14" value="4" class="rating_radio4">
						<label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating14" value="5" class="rating_radio4">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

				<br><br>
		
				<input type="button" class="button-gold" id="button-gold_estate_planinng" name="estate_plan" value="Next"/ >
			</span>
		</div> 
		<div id="insurance" class="quiz-container">
		  <h3>INSURANCE &amp; PROTECTION</h3>
		   <center>How confident are you that you have the right amount of life insurance and that you are not overpaying for that coverage?
		   </center>
		   <input type="hidden" name="hide_insurance" value="How confident are you that you have the right amount of life insurance and that you are not overpaying for that coverage? " />
		   <span class="rating5">
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating15" value="1" class="rating_radio5">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating15" value="2" class="rating_radio5">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating15" value="3" class="rating_radio5">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating15" value="4" class="rating_radio5">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating15" value="5" class="rating_radio5">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

        <hr>
		  
				<center>How confident are you that you have the right amount of long-term care insurance and that you are not overpaying for that coverage?
				</center>
				<input type="hidden" name="hide_insurance1" value="How confident are you that you have the right amount of long-term care insurance and that you are not overpaying for that coverage? " />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating16" value="1" class="rating_radio5">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating16" value="2" class="rating_radio5">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating16" value="3" class="rating_radio5">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating16" value="4" class="rating_radio5">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating16" value="5" class="rating_radio5">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

        <hr>
		  
				<center>How confident are you that you have the right amount of health insurance and are not overpaying for that coverage?
				</center>	
				<input type="hidden" name="hide_insurance2" value="How confident are you that you have the right amount of health insurance and are not overpaying for that coverage?" />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating17" value="1" class="rating_radio5">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating17" value="2" class="rating_radio5">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating17" value="3" class="rating_radio5">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating17" value="4" class="rating_radio5">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating17" value="5" class="rating_radio5">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>
            <hr>
		  
				<center>How confident are you that you have the right amount of property and liability insurance and are not overpaying for that coverage?</center>	
				<input type="hidden" name="hide_insurance3" value="How confident are you that you have the right amount of property and liability insurance and are not overpaying for that coverage?" />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating18" value="1" class="rating_radio5">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating18" value="2" class="rating_radio5">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating18" value="3" class="rating_radio5">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating18" value="4" class="rating_radio5">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating18" value="5" class="rating_radio5">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

				<hr>

				<center>Are you sure the beneficiaries listed on your life insurance policies are correct?</center>
				<input type="hidden" name="hide_insurance4" value="Are you sure the beneficiaries listed on your life insurance policies are correct" />			
				<div class="rating-group">
					<span class="rating-low">No</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating_policies" value="1" class="rating_radio5">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating_policies" value="2" class="rating_radio5">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating_policies" value="3" class="rating_radio5">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating_policies" value="4" class="rating_radio5">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating_policies" value="5" class="rating_radio5">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Yes</span>
				</div>

			    <br><br>			
			    <input type="button" id="button-gold_insurance" class="button-gold" name="insurance" value="Next"/ >
			</span>
		</div>  
		<div id="case_flow" class="quiz-container">
			<h3>CASH FLOW AND BUDGETING</h3>
		    <span class="rating6">
				<center>Do you have 6 months worth of expenses in an emergency account that allows you to sleep at night? </center>
				<input type="hidden" name="hide_case" value="Do you have 6 months worth of expenses in an emergency account that allows you to sleep at night?" />				
				<div class="rating-group">
					<span class="rating-low">No</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating_night" value="1" class="rating_radio6">
						<label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating_night" value="2" class="rating_radio6">
						<label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating_night" value="3" class="rating_radio6">
						<label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating_night" value="4" class="rating_radio6">
						<label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating_night" value="5" class="rating_radio6">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Yes</span>
				</div>

				<hr>

				<center>How confident are you that your spending, including any expenses on autopay, is optimized to eliminate any waste?</center>
				<input type="hidden" name="hide_case1" value="How confident are you that your spending, including any expenses on autopay, is optimized to eliminate any waste?" />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating19" value="1" class="rating_radio6">
						<label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating19" value="2" class="rating_radio6">
						<label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating19" value="3" class="rating_radio6">
						<label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating19" value="4" class="rating_radio6">
						<label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating19" value="5" class="rating_radio6">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

					<hr>
				<center>Does your budget have enough buffer in it that allows you to do the extra things you’d like to, without being worried if you can afford them? </center>
				<input type="hidden" name="hide_case2" value="Does your budget have enough buffer in it that allows you to do the extra things you’d like to, without being worried if you can afford them?" />				
				<div class="rating-group">
					<span class="rating-low">No</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating_afford" value="1" class="rating_radio6">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating_afford" value="2" class="rating_radio6">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating_afford" value="3" class="rating_radio6">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating_afford" value="4" class="rating_radio6">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating_afford" value="5" class="rating_radio6">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Yes</span>
				</div>
			    <hr>
				<center>When making large purchases on a credit card to maximize rewards points, how confident are you that you are using the credit cards that maximizes those potential dollars back to your pocket?</center>
				<input type="hidden" name="hide_case3" value="When making large purchases on a credit card to maximize rewards points, how confident are you that you are using the credit cards that maximizes those potential dollars back to your pocket?" />
				<div class="rating-group">
					<span class="rating-low">Unconfident</span>
					<div class="rating-input">
						<input id="rating1" type="radio" name="rating20" value="1" class="rating_radio6">
						                <label for="rating1">1</label>
					</div>
					<div class="rating-input">
						<input id="rating2" type="radio" name="rating20" value="2" class="rating_radio6">
						                <label for="rating2">2</label>
					</div>
					<div class="rating-input">
						<input id="rating3" type="radio" name="rating20" value="3" class="rating_radio6">
						                <label for="rating3">3</label>
					</div>
					<div class="rating-input">
						<input id="rating4" type="radio" name="rating20" value="4" class="rating_radio6">
						                <label for="rating4">4</label>
					</div>
					<div class="rating-input">
						<input id="rating5" type="radio" name="rating20" value="5" class="rating_radio6">
						<label for="rating5">5</label>
					</div>
					<span class="rating-high">Confident</span>
				</div>

                <br><br> 			
				<input type="submit" id="case_flow_button-gold" class="button-gold" name="submit" value="Get Your Results"/ >
		    </span>
		</div>
</form>

<?php get_footer(); ?>