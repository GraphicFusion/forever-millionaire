<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond|Montserrat:400,700" rel="stylesheet">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MB5W8WP');</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110503781-1"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());
	 gtag('config', 'UA-110503781-1');
	</script>
	
<!-- Dynamic Spinner Image Start -->	
<?php $url= get_site_url(); ?>
<script>
$(document).ready(function(){
	$("#gform_submit_button_1").click(function(){
		$('<img id="gform_ajax_spinner_1" class="gform_ajax_spinner" src="<?php echo $url; ?>/wp-content/uploads/2018/03/spinner.gif" alt="" />').insertBefore(".gform_hidden");
				
    });
});
</script>	
<!-- Dynamic Spinner Image End -->	
	<?php wp_head(); ?>
</head>