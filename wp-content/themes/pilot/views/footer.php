<footer class="site-footer">
	<div class="site-info" style="font-size:16px; text-align:center;">
		Investment advice offered through Shepherd Wealth Group, a Registered Investment Adviser doing business as Shepherd Wealth &amp; Retirement. &copy;<?php echo date("Y"); ?> No Secrets to Success, LLC 6300 E. El Dorado Plaza, Suite A200, Tucson, AZ 85715 <br> Phone 520-325-1600 Fax 520-325-9097 | <a href="/disclosures/">Disclosures</a>
	</div><!-- .site-info -->
</footer>
<!-- Facebook Pixel Code -->
<script>
 !function(f,b,e,v,n,t,s)
 {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 n.callMethod.apply(n,arguments):n.queue.push(arguments)};
 if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
 n.queue=[];t=b.createElement(e);t.async=!0;
 t.src=v;s=b.getElementsByTagName(e)[0];
 s.parentNode.insertBefore(t,s)}(window, document,'script',
 'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1969076613309470');
 fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
 src="https://www.facebook.com/tr?id=1969076613309470&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<?php
$lastpageurl = $_SERVER['HTTP_REFERER'];  
if($lastpageurl == "https://theforevermillionaire.com/order/"){
?>
<script>
$(document).ready(function(){
   $(".thank-you-box").css("display","block");
   $(".close").click(function () {
     $(".thank-you-box").css("display","none");
   });
});
</script>
  <script>
  fbq('track', 'Purchase', {
    value: 0,
    currency: 'USD',
  });
  gtag('event', 'book-thank-you', { 'send_to': 'UA-110503781-1' });

</script>

<?php
}
?>