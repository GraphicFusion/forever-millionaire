<!DOCTYPE html>
	<?php get_template_part( 'views/head' ); ?>
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MB5W8WP"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div class="site container">
			<?php get_template_part( 'views/header' ); ?>
			<div class="site-content">
				<main class="site-main">
				
				 