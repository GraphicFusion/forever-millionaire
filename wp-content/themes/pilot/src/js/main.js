(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();

jQuery(document).ready(function($) {
	$(".transcript-more").on("click", function (e) {
		e.preventDefault();
		$(".transcript").addClass("open");
		$(".transcript-more").hide();
	});

	$('.thank-you-box .close').on('click', function () {
		$('body').removeClass('green-bar');
	});

	if ( document.referrer == 'quiz page' && window.location == 'welcome page' ) {
		$('body').addClass('green-bar');
	}

	$('.video-container object').on('click', function () {
		$('.video-container').addClass('playing');
	});
});