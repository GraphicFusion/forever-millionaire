<?php

	/*
	 * Global settings for Pilot Theme
	 * The global $pilot object is defined in includes/pilot.class.php
	 *
	 * Are you adding a new function for the theme?
	 *		If you don't know where it should go, put it in includes/pilot-theme-functions.php
	 *
	 *		If it's part of the core of the theme management, place it as a Method in includes/pilot.class.php
	 *
	 **/
	require get_template_directory() . '/includes/pilot.class.php';
	$args = array(
		'sidebar' 					=> 1,		 		// set to 1 to use sidebar
		'comments'					=> 0, 				// set to 1 to use comments
		'default_admin' 			=> 1, 				// set to 0 to remove extraneous meta boxes in admin
		'use_colormaker' 			=> 1,				// set to 1 to use theme-wide color-maker plugin - not yet available
		'add_acf_options_pages' 	=> 1,				// set to 1 to add options pages
		'use_default_page_titles' 	=> 1,				// set to 1 to use default page titles (adds a hide option for indiv pages); 0 to not use default page titles;
		'include_modules' 			=> 1, 				// set to 1 to use modules
		'use_global_modules' 		=> 1,				// set to 1 to use global modules
		'module_classes' 			=> " module ",		// a string of classes that will be added to every module wrapper. i.e., ' wow fadeInUP';
		'use_environments' 			=> 1,	 			// set to 1 to use autodetect of environments
		'environments' 				=> array(
			'development' => array(						// array of development (local) environment urls
				'pilot.dev'
			),
			'staging' => array(							// array of staging (staging server) environment urls
				'pilot.wpengine.com'
			),
			'production' => array(						// array of staging (staging server) environment urls
				'pilot.wpengine.com'
			)
		)
	);
	global $pilot;
	$pilot = new Pilot($args);
	$pilot->build_pilot();						// runs after object is created; checks theme settings and requires conditionally	


	add_filter('deprecated_constructor_trigger_error', '__return_false');
add_action( 'gform_after_submission_1', 'set_post_content_form', 10, 2 );
function get_email_header($from){
	$_headers  = 'MIME-Version: 1.0' . "\r\n";
	$_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";				 
	$_headers .= 'From: '.$from."\r\n".
		'Reply-To: '.$from."\r\n" .
		'X-Mailer: PHP/' . phpversion();
	return $_headers;		

}
function set_post_content_form($entry,$form ){
	$from = "dave.shepherd@web.lpl.com";
	$_headers  = 'MIME-Version: 1.0' . "\r\n";
	$_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";				 
	$_headers .= 'From: '.$from."\r\n".
		'Reply-To: '.$from."\r\n" .
		'X-Mailer: PHP/' . phpversion();		
	$subject = "Congratulations on your book purchase";
	$first_name = $entry['5'];
	$to = $email = $entry['7'];
	$product = $entry['24.1'];
	$product_amount = $entry['24.2'];
	$total_amount = $entry['payment_amount'];
	$qty = $entry['24.3'];
	if(empty($qty)){
		$quantity = 1;	
	}else{
		$quantity = $qty;
	}	
	$message = '<html>
				<body bgcolor="#e1e1e1">
					<table class="w3-ban" width="100%" border="0" cellspacing="0" cellpadding="0" align=8c01ter">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td>						
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="scale">
											
											<tr>
												<td align="left"><a href="#"><img width="150" src="http://theforevermillionaire.com/wp-content/uploads/2018/01/logo.png"></a>
											</td>
											</tr>						  
										</table>			
									</td>
								</tr>
							</table>			
						</td>
					</tr>
				</table>
				<table class="w3-ban" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
					<td bgcolor="#e1e1e1">
					<table id="textEdit" width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
				    <tbody>
						<tr>
						<td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999; padding:14px;font-weight: bold;" align="center" valign="bottom;">Your Book Is on the Way!</td>
						</tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">Hi '.$first_name.'!</td></tr>						
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">Congratulations on ordering your copy of <i>The Forever Millionaire: Making Wise Choices with Your Wealth!</i></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">We are preparing your book for shipping and you should receive it in the next 3 business days.</td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">In this book we pull the curtains and reveal the strategies and techniques we <u>personally</u> use to manage <u>our own</u> money.</td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">The Forever Millionaire book strives to show you how to protect and grow your investments, help you stop worrying about your finances and <u>finally</u> have the freedom to focus on what matters most to <u>you</u>!</td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">You will also learn how to eliminate that feeling that you are gambling with your money, your future and the future of your loved ones.</td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">Here are some other things you can look forward to learning once you receive the book: </td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You will learn the four seasons approach of investing, designed to reduce excessive risk, limit large losses and optimize returns, so you can <u>stay</u> rich.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You will be armed with the tools you need to find someone not only with the knowledge and experience to help you manage your hard-earned money, but more importantly, that you can trust.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You will know the 4-word question that will instantly tell you if the person you are talking to has your best interest at heart, or is acting as a salesperson looking to make a commission.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You will have specific strategies we use and that you can follow to build your plan, aiming to bring order to the chaos and safeguard and grow your wealth.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You will get a checklist you can use to help you get on track and stay on track.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You will know how to eliminate the destructive effect of self-serving drive-by advice.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>The truth is that the anxiety and uncertainty you feel when you have to make a financial decision come from dangerous emotional behaviors most investors have. In The Forever Millionaire book you will learn how to spot and eliminate these bad behaviors so you can truly make better and worry-free investment decisions.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You\'ll also learn how to solve the portfolio dilemma of taking more risk to get higher returns versus facing losing it all when a down market hits. And I say when a down market hits, not if it hits!</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You\'ll learn the 4 common strategies and behaviors that most advisors follow, but that slow down your progress. We will show them, not only what they are but also how to eliminate them.</li></ul></td></tr>
						<tr><td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;"><ul><li>You\'ll discover how to spot and avoid cookie-cutter products and find tailored strategies to your unique situation.</li></ul></td></tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999; padding:14px;" valign="bottom;">
						  Also, by requesting your copy of the book you receive immediate access to a number of bonuses:
						  </td>									  
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999; padding:14px;" valign="bottom;">
							1) Our <u>Financial Clarity Assessment&trade;</u>
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999; padding:14px;" valign="bottom;">
							Maybe your financial house is in order. Maybe it needs a few tweaks. Maybe it needs a complete overhaul. Regardless of where you are, this assessment was built to help you identify areas for improvement and guide you to becoming a Forever Millionaire.
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999; padding:14px;" valign="bottom;">
							So we want to encourage you to take the <u>Financial Clarity Assessment&trade;</u> if you haven\'t already.
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							2) If you\'d like to go beyond the book and you qualify*, we want to personally invite you to attend our Investment Strategy Game Changer&trade; Workshop. This is an intimate, by invitation-only workshop, where we go deeper into the strategies and concepts you will learn in the book. Plus, you have a chance to ask any questions you may have.
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							3) If you qualify* and you are looking for a second opinion, an analysis of your portfolio, or to find out about your options, you can come to our office for your very own Financial Clarity Experience&trade;!
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							And yes, all of these are 100% complimentary and you can even take advantage of <u>all of them</u> if you\'d like.
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							Just call our office at 520-325-1600 and ask for Darlene.
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							Tell her you\'d like to register for the Investment Strategy Game Changer&trade; Workshop or schedule your Financial Clarity Experience&trade; and she will take care of you.
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							To your bigger future,
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							Dave and David Shepherd
						   </td>
						</tr>
						<tr>
						   <td style="color: #000000; font-size: 18px;  background:#fff; z-index: 999;padding:14px;" valign="bottom;">
							<img width="30%" src="http://theforevermillionaire.com/wp-content/uploads/2017/11/dave-2.jpg">
							<img width="30%" src="http://theforevermillionaire.com/wp-content/uploads/2017/11/davids-sig-4.jpg">											
						   </td>
						</tr>
						</tr>
					</tbody>
					</table>
					</td>
					</tr>
				</table>							
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td bgcolor="e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td>													
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="smocale">	
										<tr>
											<td width="100%" align="center">
												<img width="300" src="http://theforevermillionaire.com/wp-content/uploads/2018/01/logo.png">
											</td>
										</tr>
										<tr align="left">
											<td class="mail-w3" style="color:#13243d;font-size:14px;font-family:Candara;line-height:1.9em;tex-align:center;">
												*$1,000,000 or more in investable assets (excluding the value of your home) 
											</td>
										</tr>
										<tr align="left">
											<td class="mail-w3" style="color:#13243d;font-size:14px;font-family:Candara;">
												Investment advice offered through Shepherd Wealth Group, Inc., a Registered Investment Adviser doing business as Shepherd Wealth & Retirement. 
											</td>
										</tr>
										<tr align="left">
											<td class="mail-w3" style="color:#13243d;font-size:14px;font-family:Candara;">
												<a href="https://theforevermillionaire.com/disclosures/">Disclosures</a>
											</td>
										</tr>
										<tr><td class="hg1" height="20"></td></tr>
										</table>										
									</td>
								</tr>
							</table>										
					</td>
				</tr>
				</table>
				</body>
			</html>';

	$mail = wp_mail($to, $subject, $message, $_headers);
	$to="info@shepherdwealth.com,trip@sonderagency.com";
	$subject2 = "New Book Order";
	$message2 ="There has been a new order placed for the Forever Millionaire. Please visit the admin panel to review: https://theforevermillionaire.com/wp-admin/admin.php?page=gf_entries";
	$mail = wp_mail($to, $subject2, $message2, $_headers);
}
	add_action( 'wp', 'quiz_eval', 10, 2 );
	function quiz_eval(){
		if(array_key_exists('first_name', $_POST)){
			$from = "dave.shepherd@web.lpl.com";
			$_headers  = 'MIME-Version: 1.0' . "\r\n";
			$_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";				 
			$_headers .= 'From: '.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();

   		   $first_name = trim($_POST['first_name']);
		   $last_name = trim($_POST['last_name']);
		   $email = trim($_POST['email']);


	//--------------Assessment Notification----//
	$headers_notify = get_email_header($from);

	$to_notify="info@shepherdwealth.com,trip@sonderagency.com";
	$subject_notify = "New Assessment Eval Submitted";
	$message_notify ="There has been a new Assessment submitted for the Forever Millionaire. By ". $first_name." ".$last_name." at: ".$email." please visit the admin panel to review: https://theforevermillionaire.com/wp-admin/users.php?s=".$email."&action=-1&new_role&paged=1&action2=-1&new_role2";
	$notify_mail = wp_mail($to_notify, $subject_notify, $message_notify, $headers_notify);
	//------ end admin notification---//

		   $subject = '[Important] Your Financial Assessment Results';
		   $final_rating;
		   
		   $hide_investment = trim($_POST['hide_investment']);
		   $rating = trim($_POST['rating']);
		   $final_rating += $rating;
		   
		   $hide_investment1 = trim($_POST['hide_investment1']);
		   $rating1 = trim($_POST['rating1']);
		   $final_rating += $rating1;
		   
		   $hide_investment2 = trim($_POST['hide_investment2']);		   
		   $rating2 = trim($_POST['rating2']);
		   $final_rating += $rating2;
		   
		   $hide_retirement = trim($_POST['hide_retirement']);
		   $rating3 = trim($_POST['rating3']);
		   $final_rating += $rating3;
		   
		   $hide_retirement1 = trim($_POST['hide_retirement1']);
		   $rating4 = trim($_POST['rating4']);
		   $final_rating += $rating4;
		   
		   $hide_retirement2 = trim($_POST['hide_retirement2']);		 
		   $rating5 = trim($_POST['rating5']);
		   $final_rating += $rating5;
		   
		   $hide_retirement3 = trim($_POST['hide_retirement3']);
		   $rating6 = trim($_POST['rating6']);
		   $final_rating += $rating6;
		   
		   $hide_retirement4 = trim($_POST['hide_retirement4']);		 
		   $rating_list_retirement = trim($_POST['rating_list_retirement']);
		   $final_rating += $rating_list_retirement;
		   
		   $hide_taxes = trim($_POST['hide_taxes']);
		   $rating7 = trim($_POST['rating7']);
		   $final_rating += $rating7;
		   
		   $hide_taxes1 = trim($_POST['hide_taxes1']);
		   $rating8 = trim($_POST['rating8']);
		   $final_rating += $rating8;
		   
		   $hide_taxes2 = trim($_POST['hide_taxes2']);		 
		   $rating9 = trim($_POST['rating9']);
		   $final_rating += $rating9;
		   
		   $hide_taxes3 = trim($_POST['hide_taxes3']);
		   $rating_list_plan = trim($_POST['rating_list_plan']);
		   $final_rating += $rating_list_plan;
		   
		   $hide_taxes4 = trim($_POST['hide_taxes4']);
		   $rating10 = trim($_POST['rating10']);
		   $final_rating += $rating10;
		   
		   $hide_assistance = trim($_POST['hide_assistance']);
		   $rating11 = trim($_POST['rating11']);
		   $final_rating += $rating11;
		   
		   $hide_assistance1 = trim($_POST['hide_assistance1']);
		   $rating12 = trim($_POST['rating12']);
		   $final_rating += $rating12;
		   
		   $hide_assistance2 = trim($_POST['hide_assistance2']);
		   $rating13 = trim($_POST['rating13']);
		   $final_rating += $rating13;
		   
		   $hide_estate_planning = trim($_POST['hide_estate_planning']);
		   $rating_wishes = trim($_POST['rating_wishes']);
		   $final_rating += $rating_wishes;
		   
		   $hide_estate_planning1 = trim($_POST['hide_estate_planning1']);
		   $rating_die = trim($_POST['rating_die']);
		   $final_rating += $rating_die;
		   
		   $hide_estate_planning2 = trim($_POST['hide_estate_planning2']);
		   $rating14 = trim($_POST['rating14']);
		   $final_rating += $rating14;
		   
		   $hide_insurance = trim($_POST['hide_insurance']);
		   $rating15 = trim($_POST['rating15']);
		   $final_rating += $rating15;
		   
		   $hide_insurance1 = trim($_POST['hide_insurance1']);
		   $rating16 = trim($_POST['rating16']);
		   $final_rating += $rating16;
		   
		   $hide_insurance2 = trim($_POST['hide_insurance2']);
		   $rating17 = trim($_POST['rating17']);
		   $final_rating += $rating17;
		   
		   $hide_insurance3 = trim($_POST['hide_insurance3']);
		   $rating18 = trim($_POST['rating18']);
		   $final_rating += $rating18;
		   
		   $hide_insurance4 = trim($_POST['hide_insurance4']);
		   $rating_policies = trim($_POST['rating_policies']);
		   $final_rating += $rating_policies;
		   
		   $hide_case = trim($_POST['hide_case']);
		   $rating_night = trim($_POST['rating_night']);
		   $final_rating += $rating_night;
		   
		   $hide_case1 = trim($_POST['hide_case1']);
		   $rating19 = trim($_POST['rating19']);
		   $final_rating += $rating19;
		   
		   $hide_case2 = trim($_POST['hide_case2']);
		   $rating_afford = trim($_POST['rating_afford']);
		   $final_rating += $rating_afford;
		   
		   $hide_case3 = trim($_POST['hide_case3']);
		   $rating20 = trim($_POST['rating20']);
		   $final_rating += $rating20;
	       $value2 = trim($final_rating);
		  //Note: Keep it as it is in the code 
		   $values ='QUIZ SCORE: '.$final_rating.';

			YOUR INVESTMENTS
			1Q: '.$hide_investment.'
			1A: '.$rating.'

			2Q: '.$hide_investment1.'
			2A: '.$rating1.'

			3Q: '.$hide_investment2.'
			3A: '.$rating2.'


			RETIREMENT
			4Q: '.$hide_retirement.'
			4A: '.$rating3.'

			5Q: '.$hide_retirement1.'
			5A: '.$rating4.'

			6Q: '.$hide_retirement2.'
			6A: '.$rating5.'

			7Q: '.$hide_retirement3.'
			7A: '.$rating6.'

			8Q: '.$hide_retirement4.'
			8A: '.$rating_list_retirement.'

			TAXES
			9Q: '.$hide_taxes.'
			9A: '.$rating7.'

			10Q: '.$hide_taxes1.'
			10A: '.$rating8.'

			11Q: '.$hide_taxes2.'
			11A: '.$rating9.'

			12Q: '.$hide_taxes3.'
			12A: '.$rating_list_plan.'

			13Q: '.$hide_taxes4.'
			13A: '.$rating10.'

			ASSISTANCE TO LOVES ONES
			14Q: '.$hide_assistance.'
			14A: '.$rating11.'

			15Q: '.$hide_assistance1.'
			15A: '.$rating12.'

			16Q: '.$hide_assistance2.'
			16A: '.$rating13.'

			ESTATE PLANNING
			17Q: '.$hide_estate_planning.'
			17A: '.$rating_wishes.'

			18Q: '.$hide_estate_planning1.'
			18A: '.$rating_die.'

			19Q: '.$hide_estate_planning2.'
			19A: '.$rating14.'

			INSURANCE & PROTECTION
			20Q: '.$hide_insurance.'
			20A: '.$rating15.'

			21Q: '.$hide_insurance1.'
			21A: '.$rating16.'

			22Q: '.$hide_insurance2.'
			22A: '.$rating17.'
			23Q: '.$hide_insurance3.'
			23A: '.$rating18.'

			24Q: '.$hide_insurance4.'
			24A: '.$rating_policies.'

			CASH FLOW AND BUDGETING
			25Q: '.$hide_case.'
			25A: '.$rating_night.'

			26Q: '.$hide_case1.'
			26A: '.$rating19.'

			27Q: '.$hide_case2.'
			27A: '.$rating_afford.'

			28Q: '.$hide_case3.'
			28A: '.$rating20.'
			'; 
		   $user = get_users();
		   $flag = false;
		   foreach($user as $final_value){
			   $user_email = $final_value->user_email;
			   $ID = $final_value->ID;
			  if($email == $user_email){
			      $id = $ID;
				  $flag = true;
			   }
		   }
		   if($flag == true){
		       //$id = wp_update_user($first_name, $last_name, $email);
			   $update = update_user_meta($id, 'textarea',  $values);
			   if($update){
				  $value1 = "wp-admin/user-edit.php?user_id=".$id."";
			   }
		   }
		   else{
			   $id =  wp_create_user($first_name, $last_name, $email); 
			   $create = add_user_meta($id, 'textarea',  $values);
			   if($create){
				 $value1 = "wp-admin/user-edit.php?user_id=".$id."";
			   }
		   }
		    /* echo $url; */
           //API credentails
		    $Apikey = "24vyf2r43375xn5f8dxemujd";
		    $token = "cafd4fb5-cbe1-4be0-93c2-203018efb9b2"; 
		    $secret = "sy5hb5ETf9yUTZDPFc5Qh28X";
		    $list_Id = "1384874577";
			
		    $header = array('Content-Type: application/json','Authorization: Bearer '.$token);
			$api_order_url = 'https://api.constantcontact.com/v2/lists/'.$list_Id.'/contacts?api_key='.$Apikey.'';
			$ch_order = curl_init(); 
			curl_setopt( $ch_order, CURLOPT_URL, $api_order_url );
			curl_setopt($ch_order, CURLOPT_HTTPHEADER, $header);
			curl_setopt( $ch_order, CURLOPT_CUSTOMREQUEST, 'GET');
			curl_setopt( $ch_order, CURLOPT_SSL_VERIFYPEER, 0 );
			curl_setopt( $ch_order, CURLOPT_SSL_VERIFYHOST, 0 );
			curl_setopt( $ch_order, CURLOPT_RETURNTRANSFER, 1 );
			$response_order = curl_exec($ch_order );
			$result = json_decode($response_order, true);	
			
			$flag_quiz = false;
			foreach($result['results'] as $final_value){
			    $id = $final_value['id']; 
			    $email_address = $final_value['email_addresses'][0]['email_address'];
				if($email == $email_address){
				     $final_id =  $id;
					 $flag_quiz = true;
				}
			}		
			if($email != ""){				
				$data =	array(            // Contact List Array
							   'lists' =>array(
											array(
												'id' => $list_Id
											)
										),
										'email_addresses' => array(
																array(
																	'email_address' => $email
																)
															),
								'first_name' => $first_name,
								'last_name' => $last_name,							
								'custom_fields' => array(
														 array(
															'name' => 'custom_field_1',
															'value'=> $value1,
														 ),
														 array(
															'name' => 'custom_field_2',
															'value'=> $value2,
														 ),	
															 
								  )
						);
				if($flag_quiz == true){
					$entry = json_encode($data);
					$header = array('Content-Type: application/json','Authorization: Bearer '.$token);
				    $url = 'https://api.constantcontact.com/v2/contacts/'.$final_id.'?action_by=ACTION_BY_OWNER&api_key='.$Apikey.'';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$url); 
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS,$entry);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);   
					$result = curl_exec($ch);					
					$json_decode = json_decode($result, true);	
					/* echo "<pre>";
					print_r($json_decode); */				
					$status = $json_decode['status'];
					/* if($status == "ACTIVE"){
						echo "<center>Your Quiz is updated successfully !</center>";
					} */						
					curl_close($ch); 
				}
				else{
					$entry = json_encode($data);
					$header = array('Content-Type: application/json','Authorization: Bearer '.$token);
					$url = 'https://api.constantcontact.com/v2/contacts?action_by=ACTION_BY_OWNER&api_key='.$Apikey.'';
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$url); 
					curl_setopt($ch, CURLOPT_POST, true); 
					curl_setopt($ch, CURLOPT_POSTFIELDS,$entry);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);   
					$result = curl_exec($ch);
					$json_decode = json_decode($result, true);	
					
					//file_put_contents("pages.txt", print_r($json_decode));
					/* echo "<pre>";
					print_r($json_decode); */
					$status = $json_decode['status'];
					 if($status == "ACTIVE"){
						//echo "<center>Your Quiz is added successfully !</center>";
					} 
					curl_close($ch); 
				}
				
				//Send HTML EMAIL function
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'MIME-encoded: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 
				// Create email headers
				$headers .= 'From: '.$from."\r\n".
					'Reply-To: '.$from."\r\n" .
					'Bcc: Darlene@shepherdwealth.com' . "\r\n" . 
					'X-Mailer: PHP/' . phpversion();
					
				// Create preference
				$preferences = array(
					"input-charset" => "ISO-8859-1",
					"output-charset" => "UTF-8",
					"line-length" => 76,
					"line-break-chars" => "\n"
				);
				$preferences["scheme"] = "Q";
				$New_subject =  iconv_mime_encode("", $subject, $preferences);
				$New_subject = $subject;
				// Compose a simple HTML email message
				$message = '<html><head>
				<title>Email Template</title>
				<style type="text/css">
						body{
							width: 100%; 
							margin:0; 
							padding:0; 
							-webkit-font-smoothing: antialiased;
						}
						ul.dsa {
					list-style: none;
				}
						p,h1,h2,h3,h4{
							margin-top:0;
							margin-bottom:0;
							padding-top:0;
							padding-bottom:0;
						}
						html{
							width: 100%; 
						}
						a {
							color: #000;
							text-decoration: none;
						}
						.textbutton a {
							font-family: Candara, sans-serif !important;
							color: #ffffff !important;
						}
						table{
							font-size: 15px;
							border: 0;
						}
						.iebg {
							background: #ffffff;
						}
						.menulink a { color: #414a51 !important; text-decoration:none;}
						
						/* ----------- responsivity ----------- */
						@media only screen and (max-width: 768px){
							td.logo-w3l-h {
								height: 36px;
							}
							td.w3ls-ba a {
								padding: 10px 20px 12px 20px !important;
							}
							td.w3l-4h {
								height: 60px !important;
							}
							td.w3l-p{
							}
							
						}
				  @media only screen and (max-width:600px){
						
							table {
									width: 96% !important;
							}
							table.wel {
								width: 90%!important;
							}
							table.b-text {
								width: 90%!important;
							}
							td.wthree-logo a {
								font-size: 36px !important;
							}
							td.agile-main {
								font-size: 19px !important;
							}
							td.w3ls-ba {
								width: 100%!important;
								text-align: center!important;
								padding-bottom: 48px!important;
								padding-left: 20px!important;
								padding-right: 20px!important;
							}
							td.w3ls-ba2 {
								width: 100%!important;
								text-align: center!important;
								padding-left: 0px!important;
								padding-right: 42px!important;
							}
							td.w3ls-ba a, .w3ls-ba2 a {
								padding: 11px 24px 12px 24px !important;
							}
							td.w3l-3h {
								height: 54px !important;
							}
							td.w3l-p2 {
								font-size: 17px !important;
							}
							img.intr2 {
								width: 232px !important;
							}
							table.scale-center-both1 {
								width: 254px !important;
							}
							table.scale-center-both2 {
								width: 280px !important;
							}
							td.w3layouts-1 {
								font-size: 19px !important;
							}
							td.go-agile a {
								padding: 10px 20px 11px 20px !important;
							}
							table.agile1.scale {
								width: 480px !important;
							}
							td.wls-6h {
								height: 38px !important;
							}
							table.textbutton {
								width: 140px !important;
								padding: 1px !important;
							}
							table.sale2-w3.scale {
								width: 486px !important;
							}
							table.sale3-w3.scale {
								width: 405px !important;
							}
							td.w3l-he1 {
								height: 20px !important;
							}
							td.w3l-he4 table {
								width: 9% !important;
							}
							font.sale-ha {
								font-size: 25px !important;
							}
							label.sale-ha2 {
								font-size: 34px !important;
							}
							table.pro1-w3.scale {
								text-align: center;
							}
							table.right-w3.scale {
								width: 303px !important;
							}
							table.agile-pro.scale {
								width: 207px !important;
							}
							table.w3ls-pa {
								width: 250px !important;
							}
							td.w3-gr {
								height: 70px !important;
							}
							table.fa3.scale {
								width: 205px !important;
							}
							table.fa2.scale {
								width: 142px !important;
							}
							table.fas.scale {
								width: 347px !important;
							}
							table.fa1.scale {
								width: 165px !important;
							}
							td.w3l-para {
								text-align: center;
							}
							table.pro1-w3.scale {
								width: 100% !important;
							}
							table.w3-a {
								width: 36% !important;
								margin: 0 0px 0px 165px!important;
								text-align: center !important;
							}
						}
						@media only screen and (max-width:568px){
							td.wthree-logo a {
								font-size: 33px !important;
							}
							table.scale-center-both2 {
								width: 272px !important;
							}
							table.scale-center-both1 {
								width: 233px !important;
							}
							td.agile-h {
								height: 39px !important;
							}
							table.right-w3.scale {
								width: 286px !important;
							}
							table.agile-pro.scale {
								width: 195px !important;
							}
							table.fa2.scale {
								width: 152px !important;
							}
							table.fa3.scale {
								width: 179px !important;
							}
							table.fas.scale {
								width: 331px !important;
							}
							table.fa1.scale {
								width: 154px !important;
							}
							table.w3-a {
								margin: 0 0px 0px 159px!important;
							}
						}
						@media only screen and (max-width:480px){
							td.w3l-1h {
								height: 34px !important;
							}
							td.wthree-logo a {
								font-size: 32px !important;
							}
							td.agile-main {
								font-size: 18px !important;
							}
							td.logo-w3l-h {
								height: 26px;
							}
							td.w3l-2h {
								height: 41px !important;
							}
							td.w3ls-ba a, .w3ls-ba2 a {
								padding: 9px 20px 11px 20px !important;
							}
							td.w3ls-ba {
								width: 100%!important;
								text-align: center!important;
								padding-bottom: 39px!important;
								padding-left: 20px!important;
								padding-right: 29px!important;
							}
							table.agile1.scale {
								width: 378px !important;
							}
							td.w3l-4h {
								height: 43px !important;
							}
							td.w3l-p2 {
								font-size: 16px !important;
							}
							table.scale-center-both1 {
								width: 426px !important;
								text-align: center !important;
							}
							.center-w3{
								background: Transparent !important;
							}
							table.center-img {
								width: 54% !important;
							}
							table.scale-center-both1 img {
								width: 289px !important;
								text-align: center !important;
							}
							table.scale-center-both2 {
								width: 425px !important;
								text-align: center !important;
							}
							td.agile-h {
								height: 0px !important;
							}
							td.h {
								height: 22px !important;
							}
							table.sale2-w3.scale {
								width: 394px !important;
							}
							td.w3l-he2 {
								height: 25px !important;
							}
							table.sale3-w3.scale {
								width: 341px !important;
							}
							label.sale-ha2 {
								font-size: 31px !important;
							}
							font.sale-ha {
								font-size: 25px !important;
							}
							td.w3l-he9 {
								height: 25px !important;
							}
							td.produ {
								height: 25px !important;
							}
							td.noborder {
								border-left: none !important;
							}  
							table.agile-pro.scale {
								width: 405px !important;
							}
							table.right-w3.scale {
								width: 405px !important;
							}
							table.fa1.scale {
								width: 121px !important;
							}
							td.agile-main {
								font-size: 16px !important;
							}
							table.fa3.scale {
								width: 155px !important;
							}
							table.fa2.scale {
								width: 122px !important;
							}
							table.fas.scale {
								width: 277px !important;
							}
							td.go-agile {
								padding: 18px 0px 41px !important;
							}
							td.bu-w3l3 {
								height: 0px;
							}
							table.w3-a {
								margin: 0 0px 0px 135px!important;
								width: 34% !important;
							}
						}
						@media only screen and (max-width:414px){
							td.wthree-logo a {
								font-size: 30px !important;
							}
							td.w3l-1h {
								height: 18px !important;
							}
							td.agile-main {
								font-size: 15px !important;
							}
							td.logo-w3l-h {
								height: 8px !important;
							}
							td.w3l-2h {
								height: 33px !important;
							}
							td.w3ls-ba a, .w3ls-ba2 a {
								padding: 9px 18px 10px 18px !important;
							}
							td.w3ls-ba2 {
								width: 100%!important;
								text-align: center!important;
								padding-left: 0px!important;
								padding-right: 0px!important;
							}
							table.agile1.scale {
								width: 333px !important;
							}
							td.w3l-p2 {
								font-size: 15px !important;
								line-height: 27px !important;
							}
							table.scale-center-both1 {
								width: 371px !important;
							}
							table.center-img {
								width: 0% !important;
							}
							table.scale-center-both2 {
								width: 371px !important;
							}
							td.h1 {
								height: 13px !important;
							}
							td.w3layouts-1 {
								font-size: 18px !important;
							}
							.head {
								font-size: 20px !important;
								letter-spacing: 1px !important;
							}
							table.sale2-w3.scale {
								width: 323px !important;
							}
							td.w3l-he2 {
								height: 19px !important;
							}
							td.w3l-he9 {
								height: 19px !important;
							}
							td.w3l-he1 {
								height: 10px !important;
							}
							font.sale-ha {
								font-size: 22px !important;
							}
							label.sale-ha2 {
								font-size: 28px !important;
							}
							td.w3l-he6 {
								height: 12px !important;
							}
							td.scale-center-both {
								font-size: 14px !important;
							}
							table.agile-pro.scale {
								width: 346px !important;
							}
							table.right-w3.scale {
								width: 346px !important;
							}
							td.w3-hei {
								height: 22px !important;
							}
							td.height1 {
								height: 20px;
							}
							td.height3 {
								height: 11px;
							}
							td.w3-gr {
								height: 51px !important;
							}
							td.agile-main a {
								font-size: 17px !important;
							}
							table.fa3.scale {
								width: 359px !important;
								padding-top: 11px;
							}
							table.fa2.scale {
								width: 341px !important;
							}
							table.fas.scale {
								width: 227px !important;
								padding-top: 12px;
							}
							table.fa1.scale {
								width: 364px !important;
								text-align: center !important;
							}
							td.mail-w3 {
								font-size: 14px !important;
							}
							td.scale-center-both {
								line-height: 26px;
							}
							td.hg11 {
								height: 31px !important;
							}
							td.w3ls-ba {
								width: 100%!important;
								text-align: center!important;
								padding-bottom: 39px!important;
								padding-left: 58px!important;
								padding-right: 29px!important;
							}
							table.w3-a {
								margin: 0 0px 0px 115px!important;
							}
						}
						@media only screen and (max-width:384px){
							td.agile-main {
								font-size: 14px !important;
								line-height: 32px !important;
							}
							td.w3l-p2 {
								font-size: 14px !important;
							}
							table.agile1.scale {
								width: 295px !important;
							}
							td.w3l-4h {
								height: 36px !important;
							}
							td.wls-5h {
								height: 31px;
							}
							table.scale-center-both1 {
								width: 336px !important;
							}
							table.scale-center-both2 {
								width: 328px !important;
							}
							td.w3layouts-1 {
								font-size: 17px !important;
							}
							td.go-agile a {
								padding: 10px 19px 10px 19px !important;
							}
							td.w3ls-11h, td.w3ls-67h, td.w3ls-8h {
								height: 6px;
							}
							table.sale3-w3.scale {
								width: 284px !important;
							}
							font.sale-ha {
								font-size: 21px !important;
							}
							label.sale-ha2 {
								font-size: 26px !important;
							}
							td.produ {
								height: 14px !important;
							}
							table.agile-pro.scale {
								width: 321px !important;
							}
							table.right-w3.scale {
								width: 321px !important;
							}
							td.height1 {
								height: 13px;
							}
							td.mo {
								height: 38px;
							}
							td.w3l-p {
								font-size: 14px !important;
							}
							td.produ3 a, td.w3-a a {
								font-size: 15px !important;
							}
							table.w3-a {
								margin: 0 0px 0px 108px!important;
							}
							td.produ1 {
								height: 17px;
							}
						}
						@media only screen and (max-width:375px){
							table.scale-center-both1 {
								width: 325px !important;
							}
							table.right-w3.scale {
								width: 310px !important;
							}
							table.agile-pro.scale {
								width: 310px !important;
							}
							table.fa3.scale {
								width: 316px !important;
								padding-top: 5px;
							}
							table.fa2.scale {
								width: 299px !important;
							}
							table.fas.scale {
								width: 105px !important;
								padding-top: 7px;
							}
							table.fa1.scale {
								width: 312px !important;
							}
							td.wthree-logo a {
								font-size: 29px !important;
							}
							table.w3-a {
								width: 35% !important;
								margin: 0 0px 0px 103px!important;
							}
						}
						@media only screen and (max-width:320px){
							td.agile-main {
								font-size: 13px !important;
								line-height: 29px !important;
							}
							td.w3l-3h {
								height: 46px !important;
							}
							td.w3ls-ba a, .w3ls-ba2 a {
								padding: 8px 16px 10px 16px !important;
								font-size: 15px !important;
							}
							td.w3ls-ba2 {
								padding-right: 12px!important;
							}
							td.w3ls-ba {
								padding-left: 39px!important;
								padding-right: 29px!important;
							}
							td.wthree-logo a {
								font-size: 28px !important;
							}
							table.agile1.scale {
								width: 253px !important;
							}
							td.w3l-p2 {
								font-size: 13px !important;
							}
							table.scale-center-both1 {
								width: 279px !important;
							}
							table.scale-center-both2 {
								width: 280px !important;
							}
							td.w3layouts-1 {
								font-size: 16px !important;
							}
							.head {
								font-size: 18px !important;
								letter-spacing: 0px !important;
							}
							label.content {
								font-size: 14px !important;
							}
							table.sale2-w3.scale {
								width: 249px !important;
							}
							table.sale3-w3.scale {
								width: 260px !important;
							}
							font.sale-ha {
								font-size: 19px !important;
							}
							label.sale-ha2 {
								font-size: 24px !important;
							}
							td.scale-center-both {
								font-size: 13px !important;
							}
							td.w3l-p {
								font-size: 13px !important;
							}
							table.agile-pro.scale {
								width: 262px !important;
							}
							td.produ3 a, td.w3-a a {
								font-size: 14px !important;
							}
							table.right-w3.scale {
								width: 262px !important;
							}
							td.agile-main a {
								font-size: 16px !important;
							}
							table.fa1.scale {
								width: 273px !important;
							}
							table.fa2.scale {
								width: 273px !important;
							}
							table.fa3.scale {
								width: 260px !important;
								padding-top: 5px;
							}
							table.w3-a {
								width: 40% !important;
								margin: 0 0px 0px 77px!important;
							}
						}
						
					</style>
				</head>';
				$message .= '<body>';
				$message .= '<table class="w3-ban" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td>
										
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="scale">
											
											<tr>
												<td align="left"><a href="#"><img width="150" src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/logo.png"></a>
											</td>
											</tr>
										  
										</table>
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>';
				$message .='<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
										<tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											<tr><td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">Hi '.$first_name.',<br><br>

				Congratulations on successfully completing the Financial Clarity Assessment&trade;!<br><br>

				Your final score was: '.$final_rating.'<br><br>

				What does that mean and what can you do?<br><br>

				First use the scoring scale below to see where that puts you.<br><br>
				
				Then read the "Where You Want to Be" section that follows. Combined with the book, it should give you a good idea of a few simple actions you can take to get there.<br><br>
				Lastly, at the end of this email you will learn how you can get our complimentary Financial Clarity Experience&trade; where we discuss the specifics of your situation and provide you with a comprehensive analysis of your portfolio at absolutely no cost to you.<br><br>

				So let\'s get started:<br><br>

				<strong>Your final score was: '.$final_rating.'</strong><br><br></td></tr>
										
											<tr><td height="50px" style="font-size: 16px; text-transform:uppercase;font-weight:600px;">Scoring Scale:</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													<strong>28 - 44:</strong> Uh-oh! You are far below where you should be and your finances need immediate attention. It is very important that you seek professional help as soon as possible.  
				 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													<strong>45 - 64:</strong> Good start, but you are still not where you should be. There is a lot of room for optimizing your financial program - definitely seek professional help.
												</td>
											</tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both"><br>
												<strong>65 - 104: </strong>Not bad...but not great either. You have got some things in order and working but still have plenty to do to get the financial confidence you deserve.<br><br>
												<strong>105 - 124:</strong> Congratulations! You have got most things working as they should. However, there is still room for improvement and ways for you to do better. A thorough review with your financial advisor or obtaining a second opinion are good options.<br><br>
												<strong>125 - 140: </strong>  You are in awesome financial shape. You have got your financial house in great condition and the confidence that most people want. Keep at it. Great Job!
												</td>
											</tr>
										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>';

				$message .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
										
										  <tr><td height="70px" style="font-size: 1px;"><img src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/divider-1.png"></td></tr>
										<tr><td height="50px" style="font-size: 20px; text-transform:uppercase;font-weight:600px;">Where You Want to Be</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												   The assessment evaluated you on the 7 Key Financial Areas. Here\'s what each one of the 7 Key Financial Areas should look like if each was optimized as it should be:
												</td>
											</tr>
										
										 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											<tr><td><img src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/gradient.jpg"></td></tr>
											<tr><td height="50px" style="font-size: 16px; text-transform:uppercase;font-weight:600px;">Investing</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												   Great investment planning would consist of an understanding of all investment options available to you and then using sound reasoning and market experience when selecting the best options applicable to you. 
				 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px;line-height: 20px;" class="scale-center-both">
													You have a general understanding of the investing milestones necessary to reach your goals and a framework to continue learning about investing. You have a disciplined way to interact with investment results and avoid emotional decisions when under pressure. You are adaptable to changes in the financial markets.
												</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr><td height="50px" style="font-size: 16px; text-transform:uppercase;font-weight:600px;">Taxes</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												   Great income tax planning consists of understanding your tax return and how each financial decision regarding your sources of income and investing impacts your taxes. 
				 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													It also requires that you know where you stand regarding all items that would raise or lower your taxes so when changes are needed and/or opportunities arise, you have the confidence to make the best decision.<br><br> 
												</td>
											</tr>
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													You have a process for learning, reviewing and optimizing the various tax savings options available to you. 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="">&nbsp;</td></tr> 
											
											
										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>';
				$message .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
											<tr><td><img src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/iStock-183803503-sm.jpg"></td></tr>
											<tr><td height="50px" style="font-size: 16px; text-transform:uppercase;font-weight:600px;">Estate Planning</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													Great estate planning and charitable giving planning would look like this: you have all the legal documents necessary that have been reviewed recently and are appropriate for whatever matter arises: death, disability, divorce or unforeseen liability. Also, you have spent an equal amount of time and thought on preparing your beneficiaries and heirs so that any inheritance accomplishes the financial blessing that you hope to achieve.  
				 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													You have made special arrangements for any minor children or disabled beneficiaries. You have dovetailed your charitable giving strategies to maximize the impact you are trying to achieve on the final recipients of the charity\'s work, your family\'s values and your personal legacy. 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 								
											
											<tr><td height="24px" style="font-size: 16px; padding-top:24px; text-transform:uppercase;font-weight:600px;">Assistance to Loved Ones</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												<ul class="dsa">
												 Being great at assisting your loved ones is about three things:
												<li>1) Maximizing the financial impact you can have in their lives</li>
												<li>2) Making sure that their physical and mental needs are met	
												</li>
												<li>3) That 1 and 2 have the biggest possible impact on their future 
												</li>
												</ul>
				 
												</td>
											</tr>
										
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													Whether you are thinking about caring for your parents or other aging relatives you have the checklist or process necessary to facilitate what is needed.<br><br>
													For younger members of your family, your resources, advice and values are used to maximize their self reliance and to impact their future in a positive way.
												</td>
											</tr>
											
											
											<tr>
												<td height="50px" >
												   
												</td>
											</tr>
											
											
										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>';
				$message .='
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
											<tr><td><img src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/iStock-866247292-sm.jpg"></td></tr>
											<tr><td height="50px" style="font-size: 16px; text-transform:uppercase;font-weight:600px;">Retirement</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
				 Having a great retirement plan is about knowing what you need for the future and having the most personalized plan and process to get there. 

												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is also understanding the role that decisions you make now will have on your future and on the transition from work to financial freedom/retirement. 

												</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is about knowing how to maximize each action and monetary habit for your secure future. 

												</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is about making the best use of your time which is the most precious resource as it can never be replaced. 

												</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is about making sure that when you stop working, you will never be forced  to start again because your finances could not support you.

												</td>
											</tr>									
											
										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
										<tr><td class="wls-5h" height="1">&nbsp;</td></tr> 

											<tr><td height="50px" style="font-size: 16px; text-transform:uppercase;font-weight:600px;">Cash Flow and Budgeting</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
				 Great cash flow and budget management is about saving first and spending last. 

												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is about being very comfortable when comparing your expenses to your cash flow and knowing you are not behind. 
</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is about constantly becoming more financially secure because of the great financial habits you worked to develop over the years. 
</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												It is about consistently knowing which financial information is important and calls for action and follow through. For many it doesn\'t require a fanatically detailed budget. It can be as simple as saving what you need to save first, and never overspending on what\'s left over. 
</td>
											</tr>
											
											
										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>


					<!-- TOP BREAKER -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr> 

											<tr><td height="50px" style="font-size: 16px;text-transform:uppercase; font-weight:600px">Risk management and insurance planning</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													Great risk management and insurance planning is about mitigating the potential damage that unforeseen events can cause to your financial security. 
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													Whether it\'s an illness, accident, fire, theft or unexpected death, you know that you have the appropriate amount of coverage to protect you as much as possible, without making you insurance poor. 
												</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													You also work to minimize the risks you may open yourself to by exercising sound judgement, engaging in good habits, and avoiding unnecessary risks whenever possible. You have an annual assessment of any changes in your life or business that could cause you harm and you have the relationships and resources to help you cover any new or changing risks.
												</td>
											</tr>
											
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>

					<!-- INTRODUCTION -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td bgcolor="#FFFFFF">
										
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="agile1 scale">
											
											<tr><td height="70px" style="font-size: 1px;"><img src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/divider-1.png"></td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
												 Taking this assessment has given you a better understanding of your investment process. But more importantly, together with the book, it has given clarity about which pieces of the financial puzzle are missing for you.
												</td>
											</tr>
											 <tr><td class="wls-5h" height="1">&nbsp;</td></tr> 
											 <tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													Whether you are looking for an advisor, want a second opinion on your finances or want to find ways that could optimize your returns and protect your investments in a systematic and non-emotional way, get in touch with us at 520-325-1600 and ask about our complimentary Financial Clarity Experience &trade;.
												</td>
											</tr>
											<tr><td class="wls-5h" height="1">&nbsp;</td></tr>
											<tr>
												<td class="w3l-p2" style="font-family: Candara, sans-serif; color: #13243d; font-size: 14px; line-height: 20px;" class="scale-center-both">
													If you qualify*, we would be honored to look at the specifics of your situation and provide you with a comprehensive analysis of your portfolio at absolutely no cost to you.
												</td>
											</tr>
											<tr>
												<td class="w3l-p2" style="It is about consistently knowing which financial information is important and calls for action and follow through. For many it doesn�t require a fanatically detailed budget. It can be as simple as saving what you 
" class="scale-center-both">
													<br><br>To your bigger future,
												</td>
											</tr>
											<tr><td class="wls-5h" height="40">&nbsp;</td></tr> 
											<tr><td>Dave and David Shepherd</td></tr> 

<tr>
											<td>
													<img width="40%" src="http://theforevermillionaire.com/wp-content/uploads/2017/11/dave-2.jpg">
													<img width="40%" src="http://theforevermillionaire.com/wp-content/uploads/2017/11/davids-sig-4.jpg">
											</td></tr>
											<tr><td class="wls-5h" height="40">&nbsp;</td></tr> 

										</table>    
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>


					
					<!-- SALES-->
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="#e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="sale1-w3 scale">
								<tr>
									<td>                        
										<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="sale2-w3 scale">
											<tr><td class="w3l-he2" height="40">&nbsp;</td></tr> 
										   
										</table>                
									</td>
								</tr>
							</table>            
						</td>
					</tr>
				</table>


					
					<!-- FOOTER -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td bgcolor="e1e1e1">
							<table width="620" border="0" cellspacing="0" cellpadding="0" align="center" class="scale">
								<tr>
									<td>
										
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="smocale">
								
											 
											<tr>
											
											<td width="100%" align="center">
											<img width="300" src="http://shepherd.sonderdev.com/wp-content/uploads/2017/10/logo.png">
											</td>
											
											</tr>
											

												<tr align="left">
													<td class="mail-w3" style=" font-style:italics;color:#13243d;font-size:14px;font-family: Candara, sans-serif;line-height:1.9em;tex-align:center;">
														*$1,000,000 or more in investable assets (excluding the value of your home) 
													</td>
												</tr>
												<tr><td class="hg1" height="20"></td></tr>
												<tr align="left">
													<td class="mail-w3" style=" font-style:italics;color:#13243d;font-size:14px;font-family: Candara, sans-serif;">
													Investment advice offered through Shepherd Wealth Group, Inc., a Registered Investment Adviser doing business as Shepherd Wealth & Retirement.
													</td>
												</tr>
												<tr><td class="hg1" height="20"></td></tr>
												<tr>
													<td align="left" style="font-family:Candara, sans-serif; font-style:italics;color: #13243d; font-size: 14px;" class="scale-center-both">
													</td>
												</tr>
												<tr align="left">
													<td class="mail-w3" style=" font-style:italics;color:#13243d;font-size:14px;font-family: Candara, sans-serif;line-height:1.9em;tex-align:center;">
														<a href="http://shepherdwealth.com/disclosures/">Disclosures</a>
													</td>
												</tr>
												<tr><td class="hg11" height="54">&nbsp;</td></tr>
										</table>
							
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
				</table>';
				$message .= '</body>';
				$message .= '</html>';
     $from = "dave.shepherd@web.lpl.com";
     	$_headers  = 'MIME-Version: 1.0' . "\r\n";
				$_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";				 
				$_headers .= 'From: '.$from."\r\n".
					'Reply-To: '.$from."\r\n" .
					'X-Mailer: PHP/' . phpversion();
				$mail = wp_mail($email, $New_subject, $message, $_headers);
			}
	}
}		
