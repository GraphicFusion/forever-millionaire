<?php get_header(); ?>

<?php the_post(); ?>
<?php get_template_part( 'views/content', get_post_format() ); ?>

<?php get_footer(); ?>